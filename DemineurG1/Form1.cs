﻿using DemineurG1.models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DemineurG1
{
    public partial class Form1 : Form
    {
        private const int CELL_SIZE = 25;
        private Board Board;

        public Form1(Board board)
        {
            this.Board = board;
            InitializeComponent();
            InitializeBoardComponent();
        }

        private void InitializeBoardComponent()
        {
            this.Controls.Clear();
            for (int y = 0; y < Board.NB_LINES; y++)
                for (int x = 0; x < Board.NB_COLUMNS; x++)
                {
                    Cell cell = Board.getCellAt(x, y);
                    if (cell.IsVisible)
                    {
                        Label label = new Label();
                        this.Controls.Add(label);
                        label.BackColor = System.Drawing.SystemColors.ActiveCaption;
                        label.Location = new System.Drawing.Point(x * CELL_SIZE, y * CELL_SIZE);
                        label.Size = new System.Drawing.Size(CELL_SIZE, CELL_SIZE);
                        label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
                        if (cell.HasBomb)
                            label.Text = "B";
                        else if (cell.NbBombsAround > 0)
                            label.Text = cell.NbBombsAround.ToString();

                    }
                    else
                    {
                        Button button = new Button();
                        this.Controls.Add(button);
                        button.Location = new System.Drawing.Point(x * CELL_SIZE, y * CELL_SIZE);
                        button.Size = new System.Drawing.Size(CELL_SIZE, CELL_SIZE);
                        button.UseVisualStyleBackColor = true;
                        button.Click += new System.EventHandler(this.button1_Click);
                        button.Tag = cell;
                    }

                }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Button button = (Button)sender;
            Cell cell = (Cell) button.Tag;
            Board.ShowCellAt(cell.X, cell.Y);
            this.InitializeBoardComponent();
        }
    }
}
