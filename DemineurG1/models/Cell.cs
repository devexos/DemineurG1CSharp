﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemineurG1.models
{
    public class Cell
    {
        protected bool _IsVisible = false;
        protected bool _HasBomb = false;
        protected int _NbBombsAround = 0;
        private int x;
        private int y;

        public Cell(int _X, int _Y)
        {
            this.x = _X;
            this.y = _Y;
        }



        // cell.IsVisible
        public bool IsVisible { get {
                return _IsVisible;
            }
        }

        public bool HasBomb
        {
            get { return _HasBomb; }
        }


        public int NbBombsAround
        {
            get { return _NbBombsAround; }
        }

        public int X
        {
            get
            {
                return x;
            }
        }

        public int Y
        {
            get
            {
                return y;
            }
        }

        public void PlaceBomb()
        {
            _HasBomb = true;
        }

        public void Show()
        {
            _IsVisible = true;
        }

        public void IncNbBombs()
        {
            _NbBombsAround++;
        }
    }
}
