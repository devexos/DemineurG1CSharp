﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemineurG1.models
{
    public class Board
    {
        public const int NB_COLUMNS = 30;
        public const int NB_LINES = 20;
        protected Cell[,] Cells;

        public Board()
        {
            this.Cells = new Cell[NB_COLUMNS, NB_LINES];
            for(int x = 0; x < NB_COLUMNS; x++)
            {
                for(int y = 0; y < NB_LINES; y++)
                {
                    this.Cells[x, y] = new Cell(x,y);
                }
            }
            this.PlaceBombs();
        }

        private void PlaceBombs()
        {
            Random random = new Random();
            for (int i = 0; i < 60; i++)
            {
                int x = random.Next(NB_COLUMNS);
                int y = random.Next(NB_LINES);
                if (!Cells[x,y].HasBomb)
                {
                    this.PlaceBombAt(x, y);
                } else
                {
                    i--;
                }

            }
        }

        private void PlaceBombAt(int x, int y)
        {
            Cells[x, y].PlaceBomb();
            for(int dx = -1; dx <= 1; dx++)
            {
                for(int dy = -1; dy <= 1; dy++)
                {
                    int posX = x + dx;
                    int posY = y + dy;
                    if (PositionIsInBoard(posX, posY))
                    {
                        Cells[posX, posY].IncNbBombs();
                    }
                }
            }
        }

        public void ShowCellAt(int x, int y)
        {
            if (!PositionIsInBoard(x, y))
                throw new Exception("Position is not in the board");
            this.ClearAreaAt(x, y);
        }

        private void ClearAreaAt(int x, int y)
        {
            if (!PositionIsInBoard(x, y))
            {
                return;
            }
            if (Cells[x,y].IsVisible)
            {
                return;
            }
            Cells[x, y].Show();
            if (Cells[x,y].NbBombsAround > 0)
            {
                return;
            }
            ClearAreaAt(x, y - 1);
            ClearAreaAt(x + 1, y);
            ClearAreaAt(x, y + 1);
            ClearAreaAt(x - 1, y);
        }

        public Cell getCellAt(int x, int y)
        {
            if (!PositionIsInBoard(x,y))
                throw new Exception("Position is not in the board");
            return Cells[x, y];
        }

        private bool PositionIsInBoard(int x, int y)
        {
            return x >= 0 && y >= 0 && x < NB_COLUMNS && y < NB_LINES;
        }
    }
}
