﻿using DemineurG1.models;
using DemineurG1.views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DemineurG1
{
    static class Program
    {
        /// <summary>
        /// Point d'entrée principal de l'application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            
            Board board = new Board();
            //ConsoleView consoleView = new ConsoleView(board);
            //consoleView.afficher();
            //board.ShowCellAt(0, 0);
            //consoleView.afficher();
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1(board));
        }
    }
}
