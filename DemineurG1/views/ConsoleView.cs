﻿using DemineurG1.models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemineurG1.views
{
    class ConsoleView
    {
        private Board Board;

        public ConsoleView(Board board)
        {
            this.Board = board;
        }

        public void afficher()
        {
            Console.WriteLine("Board : ");
            Console.WriteLine("=======");

            for (int y = 0; y < Board.NB_LINES; y++)
            {
                for (int x = 0; x < Board.NB_COLUMNS; x++)
                {
                    Cell cell = Board.getCellAt(x, y);
                    if(cell.IsVisible)
                    {
                        if (cell.HasBomb)
                        {
                            Console.Write("B ");
                        }
                        else 
                        {
                            Console.Write(cell.NbBombsAround.ToString() + " ");
                        }
                    }
                    else {
                        Console.Write("_ ");
                    }
                }
                Console.WriteLine();
            }
        }
    }
}
